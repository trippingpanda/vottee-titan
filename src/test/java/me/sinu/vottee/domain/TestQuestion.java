//package me.sinu.vottee.domain;
//
//import com.tinkerpop.gremlin.structure.Graph;
//import com.tinkerpop.gremlin.structure.Vertex;
//import com.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
//import me.sinu.vottee.web.dao.GraphRepositoryImpl;
//import me.sinu.vottee.web.dao.QuestionRepositoryImpl;
//import me.sinu.vottee.web.dao.UserRepositoryImpl;
//import me.sinu.vottee.web.payload.LikesPayload;
//import me.sinu.vottee.web.payload.QuestionPayload;
//import me.sinu.vottee.web.payload.UserPayload;
//import org.junit.Assert;
//import org.junit.Test;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by SiJohn on 3/1/2015.
// */
//public class TestQuestion {
//
//    @Test
//    public void testCreateAndDeleteQuestion() {
//        Graph g = TinkerGraph.open();
//
//        GraphRepositoryImpl graphRepository = new GraphRepositoryImpl();
//        graphRepository.setGraph(g);
//
//        QuestionRepositoryImpl questionRepository = new QuestionRepositoryImpl();
//        questionRepository.setGraphRepository(graphRepository);
//
//        UserRepositoryImpl userRepository = new UserRepositoryImpl();
//        userRepository.setGraphRepository(graphRepository);
//
//        Vertex u1 = userRepository.addUser();
//        userRepository.setUsername(u1, "uu1");
//
//        Vertex q1 = questionRepository.addQuestion();
//        questionRepository.setDescription(q1, "qq1");
//
//        Vertex q2 = questionRepository.addQuestion();
//        questionRepository.setDescription(q2, "qq2");
//
//        userRepository.addCreater(u1, q1);
//        userRepository.addCreater(u1, q2);
//
//        Vertex u = userRepository.getUser(u1.id());
//        List<QuestionPayload> questionList = userRepository.getCreatedQuestions(u, 0, 10);
//        Assert.assertTrue(questionList.size() == 2);
//        Assert.assertTrue(questionList.stream().anyMatch(qn -> qn.getDescription().equals("qq1")));
//        Assert.assertTrue(questionList.stream().anyMatch(qn -> qn.getDescription().equals("qq2")));
//
//        questionRepository.removeQuestion(q1.id());
//        questionList = userRepository.getCreatedQuestions(u, 0, 10);
//        Assert.assertTrue(questionList.size() == 1);
//        Assert.assertTrue(questionList.stream().anyMatch(qn -> qn.getDescription().equals("qq2")));
//
//    }
//    @Test
//    public void testAnswerQuestion() {
//        Graph g = TinkerGraph.open();
//
//        GraphRepositoryImpl graphRepository = new GraphRepositoryImpl();
//        graphRepository.setGraph(g);
//
//        QuestionRepositoryImpl questionRepository = new QuestionRepositoryImpl();
//        questionRepository.setGraphRepository(graphRepository);
//
//        UserRepositoryImpl userRepository = new UserRepositoryImpl();
//        userRepository.setGraphRepository(graphRepository);
//
//        Vertex u1 = userRepository.addUser();
//        userRepository.setUsername(u1, "uu1");
//        Vertex u2 = userRepository.addUser();
//        userRepository.setUsername(u2, "uu2");
//
//        Vertex q1 = questionRepository.addQuestion();
//        questionRepository.setDescription(q1, "qq1");
//
//        userRepository.addCreater(u1, q1);
//        userRepository.addAnswersEdge(u1, q1);
//        userRepository.addAnswersEdge(u2, q1);
//
//        Vertex u = userRepository.getUser(u1.id());
//        List<QuestionPayload> questionList = userRepository.getAnsweredQuestions(u, 0, 10);
//        Assert.assertTrue(questionList.size() == 1);
//        Assert.assertTrue(questionList.stream().anyMatch(qn -> qn.getDescription().equals("qq1")));
//
//        Vertex q = questionRepository.getQuestion(q1.id());
//        List<UserPayload> userList = questionRepository.getAnsweredUsers(q, 0, 10);
//        Assert.assertTrue(userList.size() == 2);
//        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu1")));
//        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu2")));
//    }
//
//    @Test
//    public void testLikeQuestion() {
//        Graph g = TinkerGraph.open();
//
//        GraphRepositoryImpl graphRepository = new GraphRepositoryImpl();
//        graphRepository.setGraph(g);
//
//        QuestionRepositoryImpl questionRepository = new QuestionRepositoryImpl();
//        questionRepository.setGraphRepository(graphRepository);
//
//        UserRepositoryImpl userRepository = new UserRepositoryImpl();
//        userRepository.setGraphRepository(graphRepository);
//
//        Vertex u1 = userRepository.addUser();
//        userRepository.setUsername(u1, "uu1");
//        Vertex u2 = userRepository.addUser();
//        userRepository.setUsername(u2, "uu2");
//        Vertex u3 = userRepository.addUser();
//        userRepository.setUsername(u3, "uu3");
//
//        Vertex q1 = questionRepository.addQuestion();
//        questionRepository.setDescription(q1, "qq1");
//
//        userRepository.addCreater(u1, q1);
//        userRepository.addLikes(u1, q1, LikeType.LIKE);
//        userRepository.addLikes(u2, q1, LikeType.DISLIKE);
//        userRepository.addLikes(u3, q1, LikeType.LIKE);
//
//        Vertex q = questionRepository.getQuestion(q1.id());
//
//        LikesPayload like = questionRepository.getLikes(q);
//        long totalLike = like.getUp() + like.getDown();
//
//        Assert.assertEquals(1, totalLike);
//
//        userRepository.removeLikes(u2, q1);
//
//        like = questionRepository.getLikes(q);
//        totalLike = like.getUp() + like.getDown();
//        Assert.assertEquals(2, totalLike);
//    }
//
//    @Test
//    public void testVoteResult() {
//        Graph g = TinkerGraph.open();
//
//        GraphRepositoryImpl graphRepository = new GraphRepositoryImpl();
//        graphRepository.setGraph(g);
//
//        QuestionRepositoryImpl questionRepository = new QuestionRepositoryImpl();
//        questionRepository.setGraphRepository(graphRepository);
//
//        UserRepositoryImpl userRepository = new UserRepositoryImpl();
//        userRepository.setGraphRepository(graphRepository);
//
//        Vertex u1 = userRepository.addUser();
//        userRepository.setUsername(u1, "uu1");
//        u1.property(DomainConstants.GENDER, "M");
//        u1.property(DomainConstants.PROFESSION, "doctor");
//
//        Vertex u2 = userRepository.addUser();
//        userRepository.setUsername(u2, "uu2");
//        u2.property(DomainConstants.GENDER, "M");
//        u2.property(DomainConstants.PROFESSION, "engineer");
//
//        Vertex u3 = userRepository.addUser();
//        userRepository.setUsername(u3, "uu3");
//        u3.property(DomainConstants.GENDER, "M");
//        u3.property(DomainConstants.PROFESSION, "politics");
//
//        Vertex u4 = userRepository.addUser();
//        userRepository.setUsername(u4, "uu4");
//        u4.property(DomainConstants.GENDER, "F");
//        u4.property(DomainConstants.PROFESSION, "engineer");
//
//        Vertex u5 = userRepository.addUser();
//        userRepository.setUsername(u5, "uu5");
//        u5.property(DomainConstants.GENDER, "F");
//        u5.property(DomainConstants.PROFESSION, "engineer");
//
//        Vertex u6 = userRepository.addUser();
//        userRepository.setUsername(u6, "uu6");
//
//        Vertex q1 = questionRepository.addQuestion();
//        questionRepository.setDescription(q1, "qq1");
//        questionRepository.setAnswers(q1, Arrays.asList("ans0", "ans1", "ans2", "ans3", "ans4"));
//
//        userRepository.addCreater(u1, q1);
//
//        userRepository.addAnswersEdge(u1, q1).property(DomainConstants.ANSWER_ID, Arrays.asList(0));
//        userRepository.addAnswersEdge(u2, q1).property(DomainConstants.ANSWER_ID, Arrays.asList(1, 2, 3));
//        userRepository.addAnswersEdge(u3, q1).property(DomainConstants.ANSWER_ID, Arrays.asList(0));
//        userRepository.addAnswersEdge(u4, q1).property(DomainConstants.ANSWER_ID, Arrays.asList(3));
//        userRepository.addAnswersEdge(u5, q1).property(DomainConstants.ANSWER_ID, Arrays.asList(0, 3));
//        userRepository.addAnswersEdge(u6, q1).property(DomainConstants.ANSWER_ID, Arrays.asList(4));
//
//        final Map<String, Object> noFilterResult = questionRepository.getVoteResult(q1, null);
//        Assert.assertEquals(3L, noFilterResult.get("0"));
//        Assert.assertEquals(1L, noFilterResult.get("1"));
//        Assert.assertEquals(1L, noFilterResult.get("2"));
//        Assert.assertEquals(3L, noFilterResult.get("3"));
//        Assert.assertEquals(1L, noFilterResult.get("4"));
//
//        final Map<String, Object> genderFilterResult = questionRepository.getVoteResult(q1, Arrays.asList(FilterType.GENDER));
//        Assert.assertEquals(2L, ((Map)genderFilterResult.get("0")).get("M"));
//        Assert.assertEquals(1L, ((Map)genderFilterResult.get("0")).get("F"));
//        Assert.assertEquals(1L, ((Map)genderFilterResult.get("1")).get("M"));
//        Assert.assertEquals(1L, ((Map)genderFilterResult.get("2")).get("M"));
//        Assert.assertEquals(2L, ((Map)genderFilterResult.get("3")).get("F"));
//        Assert.assertEquals(1L, ((Map)genderFilterResult.get("3")).get("M"));
//        Assert.assertEquals(1L, ((Map)genderFilterResult.get("4")).get("UNKNOWN"));
//
//        final Map<String, Object> twoFilterResult = questionRepository.getVoteResult(q1, Arrays.asList(FilterType.GENDER, FilterType.PROFESSION));
//        Assert.assertEquals(1L, ((Map)((Map)twoFilterResult.get("0")).get("M")).get("doctor"));
//        Assert.assertEquals(1L, ((Map)((Map)twoFilterResult.get("0")).get("M")).get("politics"));
//        Assert.assertEquals(1L, ((Map)((Map)twoFilterResult.get("0")).get("F")).get("engineer"));
//        Assert.assertEquals(1L, ((Map)((Map)twoFilterResult.get("1")).get("M")).get("engineer"));
//        Assert.assertEquals(1L, ((Map)((Map)twoFilterResult.get("2")).get("M")).get("engineer"));
//        Assert.assertEquals(1L, ((Map)((Map)twoFilterResult.get("3")).get("M")).get("engineer"));
//        Assert.assertEquals(2L, ((Map)((Map)twoFilterResult.get("3")).get("F")).get("engineer"));
//        Assert.assertEquals(1L, ((Map)((Map)twoFilterResult.get("4")).get("UNKNOWN")).get("UNKNOWN"));
//
//    }
//
//}
