package me.sinu.vottee.domain;

import com.tinkerpop.gremlin.structure.Graph;
import com.tinkerpop.gremlin.structure.Vertex;
import com.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import me.sinu.vottee.web.dao.GraphRepositoryImpl;
import me.sinu.vottee.web.dao.UserRepositoryImpl;
import me.sinu.vottee.web.payload.UserPayload;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by SiJohn on 3/1/2015.
 */
public class TestFollows {

    @Test
    public void testFollowRelationship() throws IOException {

        Graph g = TinkerGraph.open();

        GraphRepositoryImpl graphRepository = new GraphRepositoryImpl();
        graphRepository.setGraph(g);

        UserRepositoryImpl userRepository = new UserRepositoryImpl();
        userRepository.setGraphRepository(graphRepository);

        Vertex u1 = userRepository.addUser();
        userRepository.setUsername(u1, "uu1");

        Vertex u2 = userRepository.addUser();
        userRepository.setUsername(u2, "uu2");

        Vertex u3 = userRepository.addUser();
        userRepository.setUsername(u3, "uu3");

        //Follow users
        userRepository.addFollows(u1, u2);
        userRepository.addFollows(u1, u3);
        userRepository.addFollows(u2, u3);
        userRepository.addFollows(u3, u1);
        /**
         * Now graph is
         * u1 -follows-> u2 -follows-> u3 -follows-> u1
         * u1 -follows-> u3
         */

        Vertex u = userRepository.getUser(u1.id());
        List<UserPayload> userList = userRepository.getFollowedUsers(u, 0, 10);
        Assert.assertTrue(userList.size() == 2);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu2")));
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu3")));

        userList = userRepository.getFollowers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu3")));

        u = userRepository.getUser(u2.id());
        userList = userRepository.getFollowedUsers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu3")));

        userList = userRepository.getFollowers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu1")));

        u = userRepository.getUser(u3.id());
        userList = userRepository.getFollowedUsers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu1")));

        userList = userRepository.getFollowers(u, 0, 10);
        Assert.assertTrue(userList.size() == 2);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu1")));
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu2")));

        //Unfollow a user
        userRepository.removeFollows(u1, u3);
        /**
         * Now graph is
         * u1 -follows-> u2 -follows-> u3 -follows-> u1
         */
        u = userRepository.getUser(u3.id());
        userList = userRepository.getFollowedUsers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu1")));

        userList = userRepository.getFollowers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu2")));

        u = userRepository.getUser(u1.id());
        userList = userRepository.getFollowedUsers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu2")));

        userList = userRepository.getFollowers(u, 0, 10);
        Assert.assertTrue(userList.size() == 1);
        Assert.assertTrue(userList.stream().anyMatch(user -> user.getName().equals("uu3")));
    }
}
