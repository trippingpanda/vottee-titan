package me.sinu.vottee.domain;

/**
 * Created by SiJohn on 3/1/2015.
 */
public class DomainConstants {
    public static final String USER = "user";
    public static final String QUESTION = "question";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String FOLLOWS = "follows";
    public static final String CREATES = "creates";
    public static final String CREATED = "created";
    public static final String ANSWERS = "answers";
    public static final String DESCRIPTION = "description";
    public static final String QUESTION_TYPE = "qnType";
    public static final String EXPIRY = "expiry";
    public static final String TIME = "time";
    public static final String ANSWER_ID = "ansId";
    public static final String LIKES = "likes";
    public static final String LIKE_SCORE = "score";
    public static final String HIDE_RESULTS = "hideResults";
    public static final String ANONYMOUS = "anonymous";
    public static final String AUTHOR="author";

    public static final String GENDER = "sex";
    public static final String PROFESSION = "job";
}
