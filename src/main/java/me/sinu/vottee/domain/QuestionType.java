package me.sinu.vottee.domain;

/**
 * Created by SiJohn on 3/1/2015.
 */
public enum QuestionType {
    SINGLE(0),
    MULTIPLE(1);

    private int value;

    private QuestionType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
