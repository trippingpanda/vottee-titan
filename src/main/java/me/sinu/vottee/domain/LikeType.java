package me.sinu.vottee.domain;

/**
 * Created by SiJohn on 3/1/2015.
 */
public enum LikeType {
    LIKE(1),
    DISLIKE(-1);

    private int value;

    private LikeType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
