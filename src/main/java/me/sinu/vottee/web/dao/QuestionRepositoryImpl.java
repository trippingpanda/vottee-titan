package me.sinu.vottee.web.dao;

import com.tinkerpop.gremlin.process.graph.GraphTraversal;
import com.tinkerpop.gremlin.structure.Edge;
import com.tinkerpop.gremlin.structure.Graph;
import com.tinkerpop.gremlin.structure.Vertex;
import lombok.extern.slf4j.Slf4j;
import me.sinu.vottee.domain.DomainConstants;
import me.sinu.vottee.domain.FilterType;
import me.sinu.vottee.web.payload.LikesPayload;
import me.sinu.vottee.web.payload.QuestionPayload;
import me.sinu.vottee.web.payload.UserPayload;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by SiJohn on 3/26/2015.
 */
@Component
@Slf4j
public class QuestionRepositoryImpl implements QuestionRepository {

    private GraphRepository graphRepository;

    @Autowired
    private Graph graph;

    @Autowired
    public void setGraphRepository(GraphRepository graphRepository) {
        this.graphRepository = graphRepository;
    }

    @Override
    public Vertex addQuestion() {
        return graphRepository.addVertex(DomainConstants.QUESTION);
    }

    @Override
    public Vertex getQuestion(Object id) {
        return graphRepository.getVertex(id, DomainConstants.QUESTION);
    }

    @Override
    public void removeQuestion(Object id) {
        getQuestion(id).remove();
    }

    @Override
    public String getDescription(Vertex vertex) {
        return vertex.value(DomainConstants.DESCRIPTION);
    }

    @Override
    public void setDescription(Vertex vertex, String description) {
        vertex.property(DomainConstants.DESCRIPTION, description);
    }

    @Override
    public List<String> getAnswers(Vertex vertex) {
        return vertex.value(DomainConstants.ANSWERS);
    }

    @Override
    public void setAnswers(Vertex vertex, Object[] answers) {
        vertex.property(DomainConstants.ANSWERS, answers);
    }

    @Override
    public Integer getQuestionType(Vertex vertex) {
        return vertex.value(DomainConstants.QUESTION_TYPE);
    }

    @Override
    public void setQuestionType(Vertex vertex, int questionType) {
        vertex.property(DomainConstants.QUESTION_TYPE, questionType);
    }

    @Override
    public Long getExpiry(Vertex vertex) {
        return vertex.value(DomainConstants.EXPIRY);
    }

    @Override
    public void setExpiry(Vertex vertex, long expiry) {
        vertex.property(DomainConstants.EXPIRY, expiry);
    }

    @Override
    public Boolean getHideResults(Vertex vertex) {
        final Object hideResults = graphRepository.getPropertyIfExists(vertex, DomainConstants.HIDE_RESULTS);
        if(hideResults == null) {
            return false;
        }
        return (Boolean) hideResults;
    }

    @Override
    public void setHideResults(Vertex vertex, boolean hideResults) {
        vertex.property(DomainConstants.HIDE_RESULTS, hideResults);
    }

    @Override
    public Long getCreatedTime(Vertex vertex) {
        Edge edge = graphRepository.getSingleIncomingEdge(vertex, DomainConstants.CREATES);
        if(edge == null) {
            return 0L;
        }
        return edge.value(DomainConstants.TIME);
    }

    @Override
    public List<UserPayload> getAnsweredUsers(Vertex question, long low, long high) {
        List<UserPayload> userPayloads = new ArrayList<>();
        graphRepository.getIncomingVertices(question, DomainConstants.ANSWERS, low, high).forEach(vertex -> {
            UserPayload payload = new UserPayload();
            payload.setId(String.valueOf(vertex.id()));
            payload.setName(vertex.value(DomainConstants.USERNAME));
            userPayloads.add(payload);
        });
        return userPayloads;
    }

    @Override
    public Long getAnsweredUsersCount(Vertex question) {
        return graphRepository.getIncomingVerticesCount(question, DomainConstants.ANSWERS);
    }

    @Override
    public LikesPayload getLikes(Vertex question) {
        LikesPayload payload = new LikesPayload();

        question.inE(DomainConstants.LIKES).values(DomainConstants.LIKE_SCORE).forEachRemaining(Integer.class, score -> {
            if(score > 0) {
                payload.setUp(payload.getUp() + score);
            } else {
                payload.setDown(payload.getDown() + score);
            }
        });
        return payload;
    }

    @Override
    public Object getCreatedUser(Vertex question) {
        return graphRepository.getIncomingVertices(question, DomainConstants.CREATES, 0, 1).get(0).id();
    }

    @Override
    public Map<String, Object> getVoteResult(Vertex question, List<FilterType> filters) {
        if(CollectionUtils.isEmpty(filters)) {
            return getNoFiltersResult(question);
        }

        Map<String, Object> result = new HashMap<>();
        question.inE(DomainConstants.ANSWERS).forEachRemaining(edge -> {
            final List<Integer> answerIds = edge.value(DomainConstants.ANSWER_ID);
            for(Integer answerId : answerIds) {
                String aid = String.valueOf(answerId);
                if(result.get(aid) == null) {
                    result.put(aid, new HashMap<>());
                }
                Vertex answeredUser = edge.outV().next();
                populateResult((Map<String, Object>) result.get(aid), answeredUser, filters, 0);
            }
        });
        return result;
    }

    @Override
    public QuestionPayload getQuestion(Vertex question) {
        if(question.label().equals(DomainConstants.QUESTION)){
            QuestionPayload payload = new QuestionPayload();
            payload.setAnswers(question.value(DomainConstants.ANSWERS));
            payload.setAuthor(question.value(DomainConstants.AUTHOR));
            payload.setCreated(question.value(DomainConstants.CREATED));
            payload.setDescription(question.value(DomainConstants.DESCRIPTION));
            payload.setExpiry(question.value(DomainConstants.EXPIRY));
            payload.setQuestionType(question.value(DomainConstants.QUESTION_TYPE));
            payload.setHideResults(question.value(DomainConstants.HIDE_RESULTS));
            return payload;
        }
        return null;
    }

    private Map getNoFiltersResult(Vertex question) {
        Map<String, Long> result = new HashMap<>();
        int answerSize = getAnswers(question).size();
        for(int i=0; i < answerSize; i++) {
            result.put(String.valueOf(i), 0L);
        }
        question.inE(DomainConstants.ANSWERS).forEachRemaining(edge -> {
            final List<Integer> answerIds = edge.value(DomainConstants.ANSWER_ID);
            for(Integer aid : answerIds) {
                result.put(String.valueOf(aid), result.get(String.valueOf(aid)) + 1);
            }
        });
        

        return result;
    }

    private void populateResult(Map<String, Object> result, Vertex answeredUser, List<FilterType> filters, int filterIndex) {
        if(filterIndex >= filters.size()) {
            return;
        }

        String key;
        try {
            key = answeredUser.value(filters.get(filterIndex).getFilter());
        } catch (IllegalStateException e) {
            key = "UNKNOWN";
        }

        if(filterIndex == filters.size() - 1) {
            if(result.get(key) == null) {
                result.put(key, 1L);
            } else {
                result.put(key, (long) result.get(key) + 1);
            }
        } else {
            if(result.get(key) == null) {
                result.put(key, new HashMap<>());
            }
            populateResult((Map<String, Object>) result.get(key), answeredUser, filters, filterIndex + 1);
        }
    }
}
