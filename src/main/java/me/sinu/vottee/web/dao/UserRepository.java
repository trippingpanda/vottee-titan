package me.sinu.vottee.web.dao;

import com.tinkerpop.gremlin.structure.Edge;
import com.tinkerpop.gremlin.structure.Vertex;
import me.sinu.vottee.domain.LikeType;
import me.sinu.vottee.web.payload.QuestionPayload;
import me.sinu.vottee.web.payload.UserPayload;

import java.util.List;

/**
 * Created by SiJohn on 3/15/2015.
 */
public interface UserRepository {

    Vertex addUser();

    Vertex getUser(Object id);

    String getUsername(Vertex vertex);

    void setUsername(Vertex vertex, String username);

    String getPassword(Vertex vertex);

    void setPassword(Vertex vertex, String password);

    Edge addFollows(Vertex user, Vertex userToFollow);

    void removeFollows(Vertex user, Vertex userToUnFollow);

    List<UserPayload> getFollowedUsers(Vertex user, long low, long high);

    Long getFollowedUsersCount(Vertex user);

    List<UserPayload> getFollowers(Vertex user, long low, long high);

    Long getFollowersCount(Vertex user);

    Edge addCreater(Vertex user, Vertex question);

    void removeCreater(Vertex user, Vertex question);

    List<QuestionPayload> getCreatedQuestions(Vertex user, long low, long high);

    Long getCreatedQuestionsCount(Vertex user);

    Edge addAnswersEdge(Vertex user, Vertex question);

    List<QuestionPayload> getAnsweredQuestions(Vertex user, long low, long high);

    Long getAnsweredQuestionsCount(Vertex user);

    Edge addLikes(Vertex user, Vertex likeable, LikeType type);

    void removeLikes(Vertex user, Vertex likeable);
}
