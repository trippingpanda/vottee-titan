package me.sinu.vottee.web.dao;

import com.tinkerpop.gremlin.structure.Edge;
import com.tinkerpop.gremlin.structure.Element;
import com.tinkerpop.gremlin.structure.Graph;
import com.tinkerpop.gremlin.structure.Vertex;
import lombok.extern.slf4j.Slf4j;
import me.sinu.vottee.domain.DomainConstants;
import me.sinu.vottee.web.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by SiJohn on 3/26/2015.
 */
@Component
@Slf4j
public class GraphRepositoryImpl implements GraphRepository {

    private Graph graph;

    @Autowired
    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    @Override
    public Vertex addVertex(String label) {
        Vertex v = graph.addVertex(label);
        graph.tx().commit();
        return v;
    }

    @Override
    public Vertex getVertex(Object id, String label) {
        final Optional<Vertex> vertexOptional = graph.V(id).tryNext();
        if(vertexOptional.isPresent() && vertexOptional.get().label().equals(label)) {
            return vertexOptional.get();
        }
        return null;
    }


    @Override
    public Object getPropertyIfExists(Element element, String key) {
        try {
            return element.value(key);
        } catch (Exception e) {
            log.debug("{} property doesn't exist for Element with id {}", DomainConstants.HIDE_RESULTS, element.id());
        }
        return null;
    }

    @Override
    public Edge addUnidirectionalEdge(Vertex start, Vertex end, String label) {
        Edge e =  start.addEdge(label, end, DomainConstants.TIME, DateUtils.getCurrentTimeInSeconds());
        graph.tx().commit();
        return e;
    }

    @Override
    public void removeUnidirectionalEdge(Vertex start, Vertex end, String label) {
        start.outE(label).as("edgeToDelete").inV().retain(end).back("edgeToDelete").remove();
        graph.tx().commit();
    }

    @Override
    public Edge getDirectionalEdge(Vertex start, Vertex end, String label) {
        final Optional<Object> edgeOptional = start.outE(label).as("edgeFound").inV().retain(end).back("edgeFound").tryNext();
        if(edgeOptional.isPresent() && ((Edge)edgeOptional.get()).label().equals(label)) {
            return (Edge) edgeOptional.get();
        }
        return null;
    }

    @Override
    public List<Vertex> getOutgoingVertices(Vertex start, String label, long low, long high) {
        List<Vertex> vertexList = new ArrayList<>();
        start.out(label).range(low, high).forEachRemaining(vertexList::add);
        return vertexList;
    }

    @Override
    public Long getOutgoingVerticesCount(Vertex start, String label) {
        return start.out(label).count().next();
    }

    @Override
    public List<Vertex> getIncomingVertices(Vertex start, String label, long low, long high) {
        List<Vertex> vertexList = new ArrayList<>();
        start.in(label).range(low, high).forEachRemaining(vertexList::add);
        return vertexList;
    }

    @Override
    public Long getIncomingVerticesCount(Vertex start, String label) {
        return start.in(label).count().next();
    }

    @Override
    public Edge getSingleIncomingEdge(Vertex vertex, String label) {
        final Optional<Edge> edgeOptional = vertex.inE(label).tryNext();
        if(edgeOptional.isPresent()) {
            return edgeOptional.get();
        }
        return null;
    }
}
