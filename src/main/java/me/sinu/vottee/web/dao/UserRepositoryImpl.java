package me.sinu.vottee.web.dao;

import com.tinkerpop.gremlin.structure.Edge;
import com.tinkerpop.gremlin.structure.Vertex;
import me.sinu.vottee.domain.DomainConstants;
import me.sinu.vottee.domain.LikeType;
import me.sinu.vottee.web.payload.QuestionPayload;
import me.sinu.vottee.web.payload.UserPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SiJohn on 3/15/2015.
 */
@Component
public class UserRepositoryImpl implements UserRepository {

    private GraphRepository graphRepository;

    @Autowired
    public void setGraphRepository(GraphRepository graphRepository) {
        this.graphRepository = graphRepository;
    }

    @Override
    public Vertex addUser() {
        return graphRepository.addVertex(DomainConstants.USER);
    }

    @Override
    public Vertex getUser(Object id) {
        return graphRepository.getVertex(id, DomainConstants.USER);
    }

    @Override
    public String getUsername(Vertex vertex) {
        return vertex.value(DomainConstants.USERNAME);
    }

    @Override
    public void setUsername(Vertex vertex, String username) {
        vertex.property(DomainConstants.USERNAME, username);
    }

    @Override
    public String getPassword(Vertex vertex) {
        return vertex.value(DomainConstants.PASSWORD);
    }

    @Override
    public void setPassword(Vertex vertex, String password) {
        vertex.property(DomainConstants.PASSWORD, password);
    }

    @Override
    public Edge addFollows(Vertex user, Vertex userToFollow) {
        return graphRepository.addUnidirectionalEdge(user, userToFollow, DomainConstants.FOLLOWS);
    }

    @Override
    public void removeFollows(Vertex user, Vertex userToUnFollow) {
        graphRepository.removeUnidirectionalEdge(user, userToUnFollow, DomainConstants.FOLLOWS);
    }

    @Override
    public List<UserPayload> getFollowedUsers(Vertex user, long low, long high) {
        List<UserPayload> userPayloads = new ArrayList<>();
        graphRepository.getOutgoingVertices(user, DomainConstants.FOLLOWS, low, high).forEach(vertex -> {
            UserPayload payload = new UserPayload();
            payload.setId(String.valueOf(vertex.id()));
            payload.setName(vertex.value(DomainConstants.USERNAME));
            userPayloads.add(payload);
        });
        return userPayloads;
    }

    @Override
    public Long getFollowedUsersCount(Vertex user) {
        return graphRepository.getOutgoingVerticesCount(user, DomainConstants.FOLLOWS);
    }

    @Override
    public List<UserPayload> getFollowers(Vertex user, long low, long high) {
        List<UserPayload> userPayloads = new ArrayList<>();
        graphRepository.getIncomingVertices(user, DomainConstants.FOLLOWS, low, high).forEach(vertex -> {
            UserPayload payload = new UserPayload();
            payload.setId(String.valueOf(vertex.id()));
            payload.setName(vertex.value(DomainConstants.USERNAME));
            userPayloads.add(payload);
        });
        return userPayloads;
    }

    @Override
    public Long getFollowersCount(Vertex user) {
        return graphRepository.getIncomingVerticesCount(user, DomainConstants.FOLLOWS);
    }

    @Override
    public Edge addCreater(Vertex user, Vertex question) {
        return graphRepository.addUnidirectionalEdge(user, question, DomainConstants.CREATES);
    }

    @Override
    public void removeCreater(Vertex user, Vertex question) {
        graphRepository.removeUnidirectionalEdge(user, question, DomainConstants.CREATES);
    }

    @Override
    public List<QuestionPayload> getCreatedQuestions(Vertex user, long low, long high) {
        List<QuestionPayload> questionPayloads = new ArrayList<>();
        graphRepository.getOutgoingVertices(user, DomainConstants.CREATES, low, high).forEach(vertex -> {
            QuestionPayload payload = new QuestionPayload();
            payload.setDescription(vertex.value(DomainConstants.DESCRIPTION));
            //TODO fill the payload
            questionPayloads.add(payload);
        });
        return questionPayloads;
    }

    @Override
    public Long getCreatedQuestionsCount(Vertex user) {
        return graphRepository.getOutgoingVerticesCount(user, DomainConstants.CREATES);
    }

    @Override
    public Edge addAnswersEdge(Vertex user, Vertex question) {
        return graphRepository.addUnidirectionalEdge(user, question, DomainConstants.ANSWERS);
    }

    @Override
    public List<QuestionPayload> getAnsweredQuestions(Vertex user, long low, long high) {
        List<QuestionPayload> questionPayloads = new ArrayList<>();
        graphRepository.getOutgoingVertices(user, DomainConstants.ANSWERS, low, high).forEach(vertex -> {
            QuestionPayload payload = new QuestionPayload();
            payload.setDescription(vertex.value(DomainConstants.DESCRIPTION));
            //TODO fill the payload
            questionPayloads.add(payload);
        });
        return questionPayloads;
    }

    @Override
    public Long getAnsweredQuestionsCount(Vertex user) {
        return graphRepository.getOutgoingVerticesCount(user, DomainConstants.ANSWERS);
    }

    @Override
    public Edge addLikes(Vertex user, Vertex likeable, LikeType type) {
        Edge likeEdge = graphRepository.getDirectionalEdge(user, likeable, DomainConstants.LIKES);
        if(likeEdge == null) {
            likeEdge = graphRepository.addUnidirectionalEdge(user, likeable, DomainConstants.LIKES);
        }
        likeEdge.property(DomainConstants.LIKE_SCORE, type.getValue());
        return likeEdge;
    }

    @Override
    public void removeLikes(Vertex user, Vertex likeable) {
        graphRepository.removeUnidirectionalEdge(user, likeable, DomainConstants.LIKES);
    }
}
