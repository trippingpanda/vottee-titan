package me.sinu.vottee.web.dao;

import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by SiJohn on 3/15/2015.
 */
@Component
public class UserCredentialDao {
    @Autowired
    private JdbcOperations jdbcOperations;

    public Map<String, Object> getUserCredential(String username) {
        try {
            return jdbcOperations.queryForMap("SELECT username, uid FROM USERS where username = ?", username);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public int createUserCredential(String username, String uid) {
        Preconditions.checkNotNull(uid);
        return jdbcOperations.update("INSERT INTO USERS (username, uid, enabled) VALUES (?, ?, TRUE);", username, uid);
    }
}
