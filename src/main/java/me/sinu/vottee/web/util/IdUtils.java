package me.sinu.vottee.web.util;

/**
 * Created by SiJohn on 3/27/2015.
 */
public class IdUtils {

    public static String fromId(Object id) {
        return String.valueOf(id);
    }

    public static Object toVertexId(String id) {
        return Long.valueOf(id);
    }
}
