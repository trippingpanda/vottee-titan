package me.sinu.vottee.web.util;

import java.time.ZonedDateTime;

/**
 * Created by SiJohn on 3/16/2015.
 */
public final class DateUtils {

    public static long getCurrentTimeInSeconds() {
        return ZonedDateTime.now().toEpochSecond();
    }
}
