package me.sinu.vottee.web.context;

import com.tinkerpop.gremlin.structure.Vertex;
import me.sinu.vottee.web.config.VotteeUserDetails;
import me.sinu.vottee.web.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by SiJohn on 3/15/2015.
 */
@Component
public class CurrentUserContext {

    @Autowired
    private UserRepository userRepository;

    public VotteeUserDetails getCurrentUserDetails() {
        return (VotteeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public Vertex getCurrentUser() {
        return userRepository.getUser(getCurrentUserDetails().getUid());
    }
}
