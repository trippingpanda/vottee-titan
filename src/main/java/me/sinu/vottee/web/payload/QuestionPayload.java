package me.sinu.vottee.web.payload;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by SiJohn on 3/8/2015.
 */
public class QuestionPayload {
    @XmlElement(name = "description")
    @Getter
    @Setter
    private String description;

    @XmlElement(name = "answers")
    @Getter
    @Setter
    private List<String> answers;

    @XmlElement(name = "questionType")
    @Getter
    @Setter
    private Integer questionType;

    @XmlElement(name = "expiry")
    @Getter
    @Setter
    private Long expiry;

    @XmlElement(name = "hideResults")
    @Getter
    @Setter
    private Boolean hideResults;

    @XmlElement(name = "created")
    @Getter
    @Setter
    private Long created;

    @XmlElement(name = "author")
    @Getter
    @Setter
    private String author;

}
