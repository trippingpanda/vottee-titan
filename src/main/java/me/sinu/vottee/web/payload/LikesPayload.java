package me.sinu.vottee.web.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by SiJohn on 3/8/2015.
 */
@NoArgsConstructor
@AllArgsConstructor
public class LikesPayload {
    @XmlElement(name = "up")
    @Getter
    @Setter
    private long up;

    @XmlElement(name = "down")
    @Getter
    @Setter
    private long down;
}
