package me.sinu.vottee.web.payload;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by SiJohn on 3/8/2015.
 */
public class CreateUserPayload {
    @XmlElement(name = "userId")
    @Getter
    @Setter
    private String userId;

    @XmlElement(name = "accessToken")
    @Getter
    @Setter
    private String accessToken;
}
