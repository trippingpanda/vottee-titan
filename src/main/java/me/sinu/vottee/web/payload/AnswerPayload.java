package me.sinu.vottee.web.payload;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by SiJohn on 3/8/2015.
 */
public class AnswerPayload {

    @XmlElement(name = "answerIds")
    @Getter
    @Setter
    private List<Integer> answerIds;

    /**
     * Optional field. Denotes whether answering a question is anonymous
     */
    @XmlElement(name = "anonymous")
    @Getter
    @Setter
    private Boolean anonymous;
}
