package me.sinu.vottee.web.resources;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * Created by SiJohn on 3/15/2015.
 */
@Service
@Provider
public class LoginServiceImpl implements LoginService {
    @Override
    public void login() {
    }

    @Override
    public void logout(@Context HttpServletRequest request) {
        request.getSession().invalidate();
    }
}
