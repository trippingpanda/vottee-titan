package me.sinu.vottee.web.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * Created by SiJohn on 3/8/2015.
 */
@Path("/")
public interface LoginService {

    @GET
    @Path("/login")
    void login();

    @GET
    @Path("/logout")
    public void logout(@Context HttpServletRequest request);

}
