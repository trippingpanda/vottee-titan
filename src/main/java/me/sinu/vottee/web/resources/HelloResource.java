package me.sinu.vottee.web.resources;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * Created by SiJohn on 3/7/2015.
 */
@Service
@Path("/")
public class HelloResource {
    @GET
    @Path("/hello")
    public String greet() {
        return "Hello REST!";
    }

    @GET
    @Path("/user1")
    public String user() {
        return "User: " + SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @GET
    @Path("/logout1")
    public String logout(@Context HttpServletRequest request) {
        request.getSession().invalidate();
        return "Logged Out";
    }
}
