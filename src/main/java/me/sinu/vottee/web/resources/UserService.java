package me.sinu.vottee.web.resources;

import me.sinu.vottee.web.payload.CreateUserPayload;
import me.sinu.vottee.web.payload.UserPayload;
import me.sinu.vottee.web.payload.IdPayload;

import javax.ws.rs.*;

/**
 * Created by SiJohn on 3/8/2015.
 */
@Path("/user")
public interface UserService {
    /**
     * Creates a user
     * @param createUserPayload
     * @return the userId(UUID) if user is created
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/create")
    IdPayload createUser(CreateUserPayload createUserPayload);

    @GET
    @Produces("application/json")
    @Path("/{userId}")
    UserPayload getUser(@PathParam("userId") String userId);

    @GET
    @Path("/{userId}/follow")
    void followUser(@PathParam("userId") String userId);

    @GET
    @Path("/{userId}/unfollow")
    void unFollowUser(@PathParam("userId") String userId);

}
