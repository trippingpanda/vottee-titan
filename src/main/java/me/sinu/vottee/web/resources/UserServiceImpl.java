package me.sinu.vottee.web.resources;

import com.google.common.base.Preconditions;
import com.tinkerpop.gremlin.structure.Vertex;
import me.sinu.vottee.web.context.CurrentUserContext;
import me.sinu.vottee.web.dao.UserCredentialDao;
import me.sinu.vottee.web.dao.UserRepository;
import me.sinu.vottee.web.payload.CreateUserPayload;
import me.sinu.vottee.web.payload.UserPayload;
import me.sinu.vottee.web.payload.IdPayload;
import me.sinu.vottee.web.util.IdUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import java.util.Map;

/**
 * Created by SiJohn on 3/8/2015.
 */
@Service
@Provider
public class UserServiceImpl implements UserService {

    @Autowired
    private CurrentUserContext currentUserContext;

    @Autowired
    private UserCredentialDao userCredentialDao;

    @Autowired
    private UserRepository userRepository;

    @Override
    public IdPayload createUser(CreateUserPayload createUserPayload) {
        final Map<String, Object> userDetails = userCredentialDao.getUserCredential(createUserPayload.getUserId());
        if(MapUtils.isNotEmpty(userDetails)) {
            throw new WebApplicationException("UserId already exists", 409);
        }

        Vertex user = userRepository.addUser();
        int updatedCount = userCredentialDao.createUserCredential(createUserPayload.getUserId(), IdUtils.fromId(user.id()));
        Preconditions.checkArgument(updatedCount > 0);

        userRepository.setUsername(user, "fbName" + createUserPayload.getUserId()); //TODO: update with FB name

        return new IdPayload(IdUtils.fromId(user.id()));
    }

    @Override
    public UserPayload getUser(String userId) {
        Vertex user = userRepository.getUser(IdUtils.toVertexId(userId));
        if(user == null) {
            throw new WebApplicationException("User not found", 400);
        }

        UserPayload payload = new UserPayload();
        payload.setName(userRepository.getUsername(user));
        payload.setId(String.valueOf(user.id()));
        return payload;
    }

    @Override
    public void followUser(String userId) {
        Vertex me = currentUserContext.getCurrentUser();
        Object uid = IdUtils.toVertexId(userId);
        if(me.id().equals(uid)) {
            return; //cannot follow myself!
        }

        Vertex userToFollow = userRepository.getUser(uid);
        if(userToFollow == null) {
            return;
        }
        userRepository.addFollows(me, userToFollow);
    }

    @Override
    public void unFollowUser(String userId) {
        Vertex me = currentUserContext.getCurrentUser();
        Object uid = IdUtils.toVertexId(userId);
        if(me.id().equals(uid)) {
            return; //cannot unfollow myself!
        }

        Vertex userToUnFollow = userRepository.getUser(uid);
        if(userToUnFollow == null) {
            return;
        }
        userRepository.removeFollows(me, userToUnFollow);
    }
}
