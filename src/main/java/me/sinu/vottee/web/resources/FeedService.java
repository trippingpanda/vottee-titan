package me.sinu.vottee.web.resources;

import me.sinu.vottee.web.payload.FeedPayload;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;

/**
 * Created by ARawat on 16-Apr-15.
 */


public interface FeedService {

    @POST
    @Produces("application/json")
    @Path("/feed")
    FeedPayload getQuestionFeed(@QueryParam("pageNumber")int pageNumber);
}
