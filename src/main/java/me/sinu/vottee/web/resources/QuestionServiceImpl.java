package me.sinu.vottee.web.resources;

import com.google.common.base.Preconditions;
import com.tinkerpop.gremlin.structure.Edge;
import com.tinkerpop.gremlin.structure.Vertex;
import me.sinu.vottee.domain.DomainConstants;
import me.sinu.vottee.domain.FilterType;
import me.sinu.vottee.domain.LikeType;
import me.sinu.vottee.domain.QuestionType;
import me.sinu.vottee.web.context.CurrentUserContext;
import me.sinu.vottee.web.dao.QuestionRepository;
import me.sinu.vottee.web.dao.UserRepository;
import me.sinu.vottee.web.payload.AnswerPayload;
import me.sinu.vottee.web.payload.QuestionPayload;
import me.sinu.vottee.web.payload.IdPayload;
import me.sinu.vottee.web.util.DateUtils;
import me.sinu.vottee.web.util.IdUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import java.util.*;

/**
 * Created by SiJohn on 3/16/2015.
 */
@Service
@Provider
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private CurrentUserContext currentUserContext;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public IdPayload createQuestion(QuestionPayload questionPayload) {
        validateAndSanitizeCreateQuestionPayload(questionPayload);

        Vertex question = questionRepository.addQuestion();
        questionRepository.setDescription(question, questionPayload.getDescription());
        questionRepository.setAnswers(question, questionPayload.getAnswers().toArray(new String[0]));
        questionRepository.setQuestionType(question, questionPayload.getQuestionType());
        questionRepository.setExpiry(question, questionPayload.getExpiry());
        if(questionPayload.getHideResults()) {
            questionRepository.setHideResults(question, questionPayload.getHideResults());
        }
        //TODO add question restrictions. Also validate it

        Vertex me = currentUserContext.getCurrentUser();

        userRepository.addCreater(me, question);
        return new IdPayload(IdUtils.fromId(question.id()));
    }

    private void validateAndSanitizeCreateQuestionPayload(QuestionPayload questionPayload) {
        try {
            Preconditions.checkNotNull(questionPayload);
            Preconditions.checkNotNull(questionPayload.getDescription());
            Preconditions.checkNotNull(questionPayload.getQuestionType());
            Preconditions.checkNotNull(questionPayload.getAnswers());
            Preconditions.checkNotNull(questionPayload.getExpiry());
        } catch (NullPointerException e) {
            throwValidationError("Invalid payload" + e.getMessage());
        }

        //Validate description. 10 < Description.length <= 500
//        questionPayload.setDescription(questionPayload.getDescription().trim().substring(0, 500));
        if(StringUtils.isBlank(questionPayload.getDescription())) {
            throwValidationError("Invalid description");
        }

        //Validate Question Type, Expiry
        if(questionPayload.getQuestionType() != QuestionType.SINGLE.getValue() && questionPayload.getQuestionType() != QuestionType.MULTIPLE.getValue()) {
            throwValidationError("Invalid question type");
        }
        if(questionPayload.getExpiry() < DateUtils.getCurrentTimeInSeconds() + 60*60) {//CurrentTime + 1 hour
            throwValidationError("Invalid Expiry time. Should provide at least a 1 hour voting window");
        }
        if(questionPayload.getHideResults() == null) {
            questionPayload.setHideResults(false);
        }

        //Validate Answers. Minimum 2 answers and no duplicates are allowed.
        //Maximum length of each answer is 500. No minimum length
        if(CollectionUtils.isEmpty(questionPayload.getAnswers()) || questionPayload.getAnswers().size() < 2) {
            throwValidationError("Invalid payload. Provide at least two answers.");
        }
//        for(int i = 0; i < questionPayload.getAnswers().size(); i++) {
//            String answer = questionPayload.getAnswers().get(i);
//            if(answer == null) {
//                throwValidationError("Invalid answer");
//            }
////            answer = answer.trim().substring(0, 500);
//            questionPayload.getAnswers().set(i, answer);
//        }
//        for(int i = 0; i < questionPayload.getAnswers().size() - 1; i++) {
//            for(int j = i+1; j < questionPayload.getAnswers().size(); j++) {
//                if(questionPayload.getAnswers().get(i).equals(questionPayload.getAnswers().get(j))) {
//                    throwValidationError("Duplicate answers provided");
//                }
//            }
//        }
    }

    private void throwValidationError(final String message) {
        throw new WebApplicationException(message, 400);
    }

    @Override
    public QuestionPayload getQuestion(String questionId) {
        Vertex question = tryGetQuestion(questionId);
        return questionRepository.getQuestion(question);
    }

    @Override
    public void answerQuestion(String questionId, AnswerPayload answerPayload) {
        validateAnswerPayload(answerPayload);

        Vertex question = tryGetQuestion(questionId);
        validateAnswerPayload(answerPayload, question);

        Vertex me = currentUserContext.getCurrentUser();
        //TODO check restrictions of the question.
        //If user doesn't have a field specified in the restrictions OR doesn't satisfy the restriction, throw error

        final Edge answers = userRepository.addAnswersEdge(me, question);
        answers.property(DomainConstants.ANSWER_ID, answerPayload.getAnswerIds());
        if(answerPayload.getAnonymous()) {
            answers.property(DomainConstants.ANONYMOUS, answerPayload.getAnonymous());
        }
    }

    private void validateAnswerPayload(AnswerPayload answerPayload, Vertex question) {
        for(Integer aid : answerPayload.getAnswerIds()) {
            if(aid >= questionRepository.getAnswers(question).size()) {
                throwValidationError("Invalid answerId. Exceeded maximum allowed value for the question");
            }
        }

        final Integer questionType = questionRepository.getQuestionType(question);
        if(questionType == QuestionType.SINGLE.getValue() && answerPayload.getAnswerIds().size() > 1) {
            throwValidationError("Only a single answerId is allowed for this question");
        }

        if(DateUtils.getCurrentTimeInSeconds() > questionRepository.getExpiry(question)) {
            throwValidationError("Question expired");
        }
    }

    private void validateAnswerPayload(AnswerPayload answerPayload) {
        try {
            Preconditions.checkNotNull(answerPayload);
            Preconditions.checkNotNull(answerPayload.getAnswerIds());
        } catch (NullPointerException e) {
            throwValidationError("Invalid payload");
        }

        if(answerPayload.getAnonymous() == null) {
            answerPayload.setAnonymous(false);
        }

        if(CollectionUtils.isEmpty(answerPayload.getAnswerIds())) {
            throwValidationError("Atleast one answer id must be provided");
        }
        for(Integer aid : answerPayload.getAnswerIds()) {
            if(aid == null || aid < 0) {
                throwValidationError("Invalid answer id");
            }
        }
    }

    @Override
    public void upVoteQuestion(String questionId) {
        setLike(questionId, LikeType.LIKE);
    }

    @Override
    public void downVoteQuestion(String questionId) {
        setLike(questionId, LikeType.DISLIKE);
    }

    @Override
    public Map<String, Object> getVoteResult(String questionId, String filters) {
        Vertex question = tryGetQuestion(questionId);
        if(questionRepository.getHideResults(question) && DateUtils.getCurrentTimeInSeconds() < questionRepository.getExpiry(question)) {
            //Question has hideResults property and the question is not expired. So return error message
            Map<String, Object> errorMap = new HashMap<>();
            errorMap.put("ERROR", "Results are hidden");
            return errorMap;
        }

        if(filters == null) {
            filters = "";
        }
        filters = filters.toLowerCase();
        List<String> filterList = Arrays.asList(filters.split("-"));
        LinkedHashSet<FilterType> filterTypes = new LinkedHashSet<>(filterList.size());
        filterList.forEach(filterString -> {
            final FilterType filterType = FilterType.get(filterString);
            if(filterType != null) {
                filterTypes.add(filterType);
            }
        });
        return questionRepository.getVoteResult(question, new ArrayList<>(filterTypes));
    }

    private void setLike(String questionId, LikeType likeType) {
        Vertex question = tryGetQuestion(questionId);
        Vertex me = currentUserContext.getCurrentUser();

        userRepository.addLikes(me, question, likeType);
    }

    private Vertex tryGetQuestion(String questionId) {
        Vertex question = questionRepository.getQuestion(IdUtils.toVertexId(questionId));
        if(question == null) {
            throw new WebApplicationException("Question not found", 404);
        }
        return question;
    }
}
