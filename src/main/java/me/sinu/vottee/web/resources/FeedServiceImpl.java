package me.sinu.vottee.web.resources;

import com.tinkerpop.gremlin.process.graph.GraphTraversal;
import com.tinkerpop.gremlin.structure.Edge;
import com.tinkerpop.gremlin.structure.Vertex;
import me.sinu.vottee.domain.DomainConstants;
import me.sinu.vottee.web.context.CurrentUserContext;
import me.sinu.vottee.web.dao.GraphRepository;
import me.sinu.vottee.web.dao.QuestionRepository;
import me.sinu.vottee.web.dao.UserRepository;
import me.sinu.vottee.web.payload.FeedPayload;
import me.sinu.vottee.web.payload.QuestionPayload;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARawat on 16-Apr-15.
 */
public class FeedServiceImpl implements FeedService{

    @Autowired
    private CurrentUserContext currentUserContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private GraphRepository graphRepository;

    @Override
    public FeedPayload getQuestionFeed(int pageNumber) {
        Vertex user = currentUserContext.getCurrentUser();
        userRepository.getUser(user.id());

        Vertex userVertex = graphRepository.getVertex(user.id(), DomainConstants.USER);

        List<Vertex> followers = userVertex.outE(DomainConstants.FOLLOWS).inV().toList();

        List<QuestionPayload> questionPayloads = collectFollowersQuestions(followers);

        questionPayloads = questionPayloads.subList((pageNumber-1)*10,(pageNumber*10)-1);

        FeedPayload feedPayload = new FeedPayload();
        feedPayload.setQuestions(questionPayloads);
        return feedPayload;
    }

    private List<QuestionPayload> collectFollowersQuestions(List<Vertex> followers) {
        List<QuestionPayload> questionPayloads = new ArrayList<>();
        followers.stream().forEach(x -> {
            x.outE(DomainConstants.CREATES).inV().toList().stream().forEach(y -> questionPayloads.add(questionRepository.getQuestion(y)));
        });

        return questionPayloads;
    }
}
