package me.sinu.vottee.web.config;

import me.sinu.vottee.web.util.IdUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Created by SiJohn on 3/9/2015.
 */
public class VotteeUserDetails extends User {

    private Object uid;

    public VotteeUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, String uid) {
        super(username, password, authorities);
        this.uid = IdUtils.toVertexId(uid);
    }

    public Object getUid() {
        return uid;
    }
}
