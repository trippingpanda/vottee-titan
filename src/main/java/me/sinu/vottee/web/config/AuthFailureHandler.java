package me.sinu.vottee.web.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by SiJohn on 3/8/2015.
 */
@Component
public class AuthFailureHandler extends BasicAuthenticationEntryPoint {
    AuthFailureHandler() {
        setRealmName("VotteeApp");
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        super.commence(request, response, authException);
        if(request.getSession(false) != null) {
            request.getSession().invalidate();
        }
    }

}
