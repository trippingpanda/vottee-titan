package me.sinu.vottee.web.config;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
import org.springframework.stereotype.Component;

@Component
public class Initializer extends AbstractHttpSessionApplicationInitializer {

}
