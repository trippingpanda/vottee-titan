package me.sinu.vottee.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Created by SiJohn on 3/9/2015.
 */
@Component
public class VotteeUserDetailsService implements UserDetailsService {
    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Map<String, Object> userMap = jdbcOperations.queryForMap("SELECT username, uid, password, enabled FROM USERS where username = ?", username);
        if(userMap == null || userMap.isEmpty() || !(Boolean)userMap.get("enabled")) {
            throw new UsernameNotFoundException("username not found/enabled");
        }

        //final Map<String, Object> authoritiesMap = jdbcOperations.queryForMap("SELECT u.username, a.authority FROM USERS u, AUTHORITIES a WHERE u.username = a.username AND u.username = ?", username);

        return getVotteeUser(userMap);
    }

    private User getVotteeUser(Map<String, Object> userMap) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

        final String username = (String) userMap.get("username");
        final String password = (String) userMap.get("password");
        final String uid = (String) userMap.get("uid");

        return new VotteeUserDetails(username, password, authorities, uid);
    }
}

